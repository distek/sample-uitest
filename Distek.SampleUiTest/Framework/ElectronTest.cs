﻿using Laurus.UiTest;
using Laurus.UiTest.Selenium;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.SampleUiTest.Framework
{
   public class ElectronTest : ITest
   {
      public ElectronTest(Func<Type, bool> mapSelector)
      {
         ChromeOptions o = new ChromeOptions();
         o.AddArgument("no-sandbox");
         o.BinaryLocation = @"C:\Users\ngamroth\AppData\Local\Programs\vt-server\vt-server.exe";
         var caps = new DesiredCapabilities();
         caps.SetCapability(CapabilityType.BrowserName, "Chrome");
         caps.SetCapability("chromeOptions", o);
         _driver = new ChromeDriver(@"C:\bin\", o);
         // This is the maximum amount of time the test should wait for a condition to be true
         // Note that this is set to about how long it takes to upload our object pool
         // If the pool is saved in non-volatile memory, this could be shortened, or a special
         // wait function could be written for the pool upload.
         _driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(60));

         var controlReg = new ControlRegistry(new[] { _driver }) as IControlRegistry;
         controlReg.RegisterLocalControls();
         _pageFactory = new PageFactory(new LocatorFactory(), controlReg, mapSelector);
      }

      public T GetPage<T>() where T : IPage
      {
         return _pageFactory.GetPage<T>();
      }

      public void ActivateApplication()
      {
         //_driver.FindElement(By.ClassName("menu-right")).FindElement(By.Id("ws-128")).Click();
         _driver.FindElement(By.Id("ws-128")).Click();
      }

      public void Navigate(string target)
      {
         // this is a major hack because of the left/right sofkey menu we get duplicate ids
         if(target == "ws-128")
         {
            this.ActivateApplication();
         }
         else if (target == "home")
         {
            _driver.FindElement(By.ClassName("menu-right")).FindElement(By.ClassName("sk-sys-right")).Click();
         }
         else if(target == "MainPage")
         {
            _driver.FindElement(By.ClassName("menu-right")).FindElement(By.ClassName("sk-0")).Click();
         }
         else if(target == "DiagnosticsPage")
         {
            _driver.FindElement(By.ClassName("menu-right")).FindElement(By.ClassName("sk-1")).Click();
         }
         else if(target == "Setup1Page")
         {
            _driver.FindElement(By.ClassName("menu-right")).FindElement(By.ClassName("sk-2")).Click();
         }
         else if(target == "Setup2Page")
         {
            _driver.FindElement(By.ClassName("menu-right")).FindElement(By.ClassName("sk-3")).Click();
         }
         else if(target == "FieldSummaryPage")
         {
            _driver.FindElement(By.ClassName("menu-right")).FindElement(By.ClassName("sk-4")).Click();
         }
      }

      public void Quit()
      {
         _driver.Quit();

      }

      public void RunScript(string script, Dictionary<string, object> parameters)
      {
      }

      public void SetOrientation(Orientation orientation)
      {
      }

      public string Source()
      {
         return "";
      }

      public void TakeScreenshot(string path)
      {
      }

      public string TakeScreenshot()
      {
         return "";
      }

      private readonly IWebDriver _driver;
      private readonly PageFactory _pageFactory;
   }
}
