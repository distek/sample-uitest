﻿using Distek.SampleUiTest.Screens;
using Laurus.UiTest;
using Laurus.UiTest.Selenium;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Distek.SampleUiTest.Framework
{
   // CollectionDefinition makes it so that all tests run during the same session.  
   // ie. the client does not reconnect between individual tests
   [CollectionDefinition("ClientTests")]
   public class ClientTestCollection : ICollectionFixture<TestFixture> { }

   public class TestFixture : IDisposable
   {
      public ITest Test { get; set; }
      public TestFixture()
      {
         // It's possible to run the tests on a mobile device, but this hasn't been tested recently
#if ios
         Test = new TestBuilder()
            .WithCapability("noReset", "true")
            .WithCapability("platformName", "iOS")
            //.WithCapability("platformVersion", "10.3")
            .WithCapability("platformVersion", "9.3")
            .WithCapability("deviceName", "iPad Air 2")
            .WithCapability("automationName", "XCUITest")
            .WithCapability("orientation", "LANDSCAPE")
            .WithCapability("app", "/var/jenkins/workspace/VT-Server-iOS/src/Distek.Vt.Mobile.iOS/bin/iPhoneSimulator/Debug/DistekVtMobileiOS.app")
            //.WithCapability("app", "/Users/distekbuild/Library/Caches/Xamarin/mtbs/builds/Distek.Vt.Mobile.iOS/7c87e89de961bdbd26287611c1f70a76/bin/iPhoneSimulator/Debug/DistekVtMobileiOS.app")
            .ImplicitWait(TimeSpan.FromSeconds(30))
            .UsingSeleniumRemote("http://192.168.200.119:4723/wd/hub");
#endif
         // web
         //Test = new TestBuilder()
         //   .WithFirefox()
         //   .SelectMapsBy(x => true)
         //   .UsingSeleniumDriver()
         //   .StartAt("http://localhost:8000");

         // Desktop app
         Test = new ElectronTest((x) => true);
         // this will start the client once per class
         Test.StartVtClient();
      }


      public void Dispose()
      {
         Test.Quit();
         // A cleaner way to shut down the client would be nice
         var p = Process.GetProcessesByName("SampleVIRTECSolution");
         if (p.Any())
            p.First().Kill();
         // this doesn't work because the client uses _getch()...
         //p.First().StandardInput.Write("\r\n");
      }
   }

   public static class TestExtensions
   {
      public static bool StartVtClient(this ITest test)
      {
         var psi = new ProcessStartInfo()
         {
            FileName = @"C:\workspace\virtec-samples\SampleVIRTECSolution\Debug\SampleVIRTECSolution.exe",
         };
         var process = Process.Start(psi);
         return true;
      }
   }
}
