using Distek.SampleUiTest.Framework;
using Distek.SampleUiTest.Screens;
using Laurus.UiTest;
using System;
using Xunit;

namespace Distek.SampleUiTest
{
   [Collection("ClientTests")]

   public class StartupTests
   {
      /// <summary>
      /// Simple test to validate that our client application has uploaded and is present
      /// in the working set menu
      /// </summary>
      [Fact]
      public void ApplicationWorkingSetIconAppears()
      {
         var wsMenu = _test.GetPage<IWorkingSetMenu>();
         _test.Navigate("ws-128");
         _test.Navigate("home");
      }

      /// <summary>
      /// Test that validates the value of a displayed output number
      /// </summary>
      [Fact]
      public void ApplicationReportsCorrectVtVersion()
      {
         // Navigate to the application under test
         var wsMenu = _test.GetPage<IWorkingSetMenu>();
         _test.Navigate("ws-128");

         // Get a reference to the metrics screen data
         var metrics = _test.GetPage<IMetricsScreen>();
         // Ask the current page for the value of the VT Version
         var version = metrics.VtVersion.Text;
         // Validate that the actual value matches our expectation
         // Note that all values are interpreted as strings, so you may have to parse them
         Assert.Equal("3", version);

         _test.Navigate("home");
      }

      /// <summary>
      /// A more sophisticated test that verifies behavior triggered
      /// from a button press
      /// </summary>
      [Fact]
      public void MeterIncreaseIncrementsSettingValue()
      {
         var wsMenu = _test.GetPage<IWorkingSetMenu>();
         _test.Navigate("ws-128");

         // Navigate to one of the other data masks via softkey press
         var sk = _test.GetPage<ISoftKeys>();
         sk.Meter.Click();

         var meterPage = _test.GetPage<IMeterPage>();
         // Read the initial value in the "Setting" input number
         var before = Int32.Parse(meterPage.Setting.Text);

         // Click the "Increase" button which will cause the ECU to increment "Setting"
         meterPage.Increase.Click();

         // This is not a great way to test, but VT clients update data a bit slower
         // than web applications usually do, so we're doing a manual wait of up to
         // 10 seconds to observe the value change.
         var passed = false;
         for (var i = 0; i < 10; i++)
         {
            // Read the updated value
            var after = Int32.Parse(meterPage.Setting.Text);
            if (after == before + 1)
            {
               passed = true;
               break;
            }
            System.Threading.Thread.Sleep(1000);
         }
         Assert.True(passed, "Meter setting failed to update from increment button press");

         _test.Navigate("home");

      }

      public StartupTests(TestFixture fixture)
      {
         _test = fixture.Test;
      }

      private ITest _test;
   }
}
