﻿using Laurus.UiTest;
using Laurus.UiTest.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.SampleUiTest.Screens
{
   public interface IMeterPage : IPage
   {
      IClickable Increase { get; }
      IClickable Decrease { get; }
      IEditable Setting { get; }
   }

   public class MeterPageMap : PageMap<IMeterPage>
   {
      public MeterPageMap()
      {
         AddToMap(x => x.Increase, LocatorKey.Id, "6004");
         AddToMap(x => x.Decrease, LocatorKey.Id, "6005");
         AddToMap(x => x.Setting, LocatorKey.Id, "9002");
      }
   }
}
