﻿using Laurus.UiTest;
using Laurus.UiTest.Controls;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.SampleUiTest.Screens
{
   public interface IMetricsScreen : IPage
   {
      IStatic VtVersion { get; set; }
   }

   public class MetricsScreenPageMap : PageMap<IMetricsScreen>
   {
      public MetricsScreenPageMap()
      {
         AddToMap(x => x.VtVersion, LocatorKey.Id, "12001");
      }
   }
}
