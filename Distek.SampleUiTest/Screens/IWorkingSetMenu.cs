﻿using Laurus.UiTest;
using Laurus.UiTest.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.SampleUiTest.Screens
{
   public interface IWorkingSetMenu : IPage
   {
      IClickable ClientIcon { get; set; }
   }

   public class WorkingSetMenuScreenMap : PageMap<IWorkingSetMenu>
   {
      public WorkingSetMenuScreenMap()
      {
         // 128 is the source address of the application under test
         // the working set menu creates elements with the id set to ws-{SA}
         AddToMap(x => x.ClientIcon, "id", "ws-128");
      }
   }
}
