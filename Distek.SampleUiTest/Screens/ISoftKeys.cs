﻿using Laurus.UiTest;
using Laurus.UiTest.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.SampleUiTest.Screens
{
   public interface ISoftKeys : IPage
   {
      IClickable Metrics { get; }
      IClickable Tank { get; }
      IClickable Meter { get; }
      IClickable Message { get; }
      IClickable NextVt { get; }
   }

   public class SoftKeysPageMap : PageMap<ISoftKeys>
   {
      public SoftKeysPageMap()
      {
         AddToMap(x => x.Metrics, LocatorKey.Id, "5000");
         AddToMap(x => x.Tank, LocatorKey.Id, "5001");
         AddToMap(x => x.Meter, LocatorKey.Id, "5002");
         AddToMap(x => x.Message, LocatorKey.Id, "5003");
         AddToMap(x => x.NextVt, LocatorKey.Id, "5004");
      }
   }
}
